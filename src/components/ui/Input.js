import styled from 'styled-components';

export const Input = styled.input`
  width: 700px;
  padding: 10px;
  font: inherit;
  padding: 7.5px 10px;
  box-sizing: border-box;
  border-radius: 5px;
  border: 1px solid #e1e3e6;
  color: hsl(0, 0%, 20%);
  outline-color: #2684ff;
`;
