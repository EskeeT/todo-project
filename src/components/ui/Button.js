import styled from 'styled-components';

export const Button = styled.button`
  border: 1px solid #e1e3e6;
  font-size: 20px;
  line-height: 1.5;
  transition: all 150ms ease;
  cursor: pointer;
  border-radius: 5px;
  box-shadow: inset 0px 0px 2px #303234;
  background-color: white;
  height: auto;
  width: auto;
  padding: 8px;

  &:hover {
    border-color: #3fa2ff;
    background-color: #3fa2ff;
    color: white;
  }
  &:focus {
    border: 1px solid #3fa2ff;
  }
  &:active {
    color: white;
    box-shadow: 0px;
    background-color: #2684ff;
    border: 1px solid #2684ff;
  }
  &[disabled] {
    cursor: not-allowed;
    text-shadow: none;
    background-color: white;
    border-color: #e1e3e6;
    color: #e1e3e6;
  }
`;
