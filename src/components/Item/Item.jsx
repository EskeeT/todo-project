import { useState, useEffect } from "react";

import SvgDelete from '../../assets/icons/Delete.svg';
import SvgEdit from '../../assets/icons/Edit.svg';
import SvgClose from '../../assets/icons/Close.svg';
import { Input } from "../ui/Input";
import { Button, } from "../ui/Button";

import { Container, Box, Span, WrapperIcon, WrapperButton, Label } from "./styled"

export const Item = ({
  updateTodoItem,
  removeTodoItem,
  completeTodoItem,
  sortTodoItem,
  task,
  id,
  isCompleted,
}) => {
  const [updateText, setUpdateText] = useState('');
  const [isEditMode, setEditMode] = useState(false);

  useEffect(() => {
    const timerId = setTimeout(() => sortTodoItem(), 1000);
    return () => clearTimeout(timerId);
  }, [isCompleted, sortTodoItem])

  const disabledSend = task === updateText || updateText === '';

  return (
    <Container>
      <Box>
        <Label>
          <input style={{ height: '18px', width: '18px' }} type="checkbox" checked={isCompleted} onChange={() => completeTodoItem(id)} />
          <Span>{task}</Span>
        </Label>
        <WrapperButton>
          <Button
            onClick={() => {
              setUpdateText(task);
              setEditMode(!isEditMode)
            }}
          >
            <WrapperIcon>
              {isEditMode ? <SvgClose /> : <SvgEdit />}
            </WrapperIcon>
          </Button>
          <Button
            onClick={() => removeTodoItem(id)}
          >
            <WrapperIcon>
              <SvgDelete />
            </WrapperIcon>
          </Button>
        </WrapperButton>
      </Box>
      {isEditMode &&
        <Box style={{ marginTop: '0px', marginLeft: "60px" }} >
          <Input
            style={{ width: '350px' }}
            autoFocus
            onChange={(e) => setUpdateText(e.currentTarget.value)}
            value={updateText}
          />
          <WrapperButton>
            <Button
              disabled={disabledSend}
              style={{ marginLeft: '30px' }}
              onClick={() => {
                updateTodoItem(updateText, id);
                setEditMode(false);
              }}
            >
              Сохранить
            </Button>
          </WrapperButton>
        </Box>
      }
    </Container>
  )
}