import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  flex-direction: column;
`;

export const Box = styled.div`
  display: flex;
  margin: 15px 20px;
  justify-content: flex-start;
  width: auto;
`;

export const Span = styled.span`
  margin-left: 20px;
  font-size: 20px;
  word-break: break-word;
  width: calc(100% - 40px);
  display: flex;
  align-items: center;
`;

export const WrapperIcon = styled.div`
  display: flex;
  width: 30px;
  margin-top: 3px;
  height: 30px;
  justify-content: center;
`;
export const WrapperButton = styled.div`
  display: flex;
  align-items: center;
  button:first-child {
    margin-right: 20px;
  }
`;

export const Label = styled.label`
  width: 100%;
  height: auto;
  display: flex;
  align-items: center;
  cursor: pointer;
`;
