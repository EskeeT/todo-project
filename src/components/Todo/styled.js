import styled from 'styled-components';

export const Box = styled.div`
  display: flex;
  margin: 20px 30px;
  justify-content: flex-start;
`;

export const Wrapper = styled(Box)`
  justify-content: center;
`;

export const WrapperButton = styled.div`
  display: flex;
  width: calc(100% - 400px);
  justify-content: flex-end;
`;

export const Span = styled.span`
  font-size: 18px;
  margin: 0 30px;
  display: flex;
  align-items: center;
`;

export const Container = styled.div`
  border-top: 2px solid #e1e3e6;
  > *:nth-child(odd) {
    background-color: ${({ odd }) => (odd === 'true' ? '#e1e3e6;' : 'white')};
  }

  > *:nth-child(even) {
    background-color: ${({ even }) => (even === 'true' ? '#e1e3e6;' : 'white')};
  }
`;
export const WrapperIcon = styled.div`
  display: flex;
  margin-top: 2px;
  justify-content: center;
`;
