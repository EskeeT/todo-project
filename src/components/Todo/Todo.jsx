import { useState, useEffect } from "react";
import { observer } from "mobx-react-lite";
import { toJS } from 'mobx';

import { Item } from "../Item";
import ItemStore from "../../store/item-store";
import { Button } from "../ui/Button";
import { Input } from "../ui/Input";
import SvgClose from '../../assets/icons/Close.svg';

import { Box, Wrapper, WrapperButton, Container, Span, WrapperIcon } from "./styled";

export const Todo = observer(() => {
  const { items, addTodoItem, removeTodoItem, updateTodoItem, completeTodoItem, sortTodoItem, removeFirstItem, removeLastItem } = ItemStore;

  const [text, setText] = useState('');
  const [isOdd, setOdd] = useState(false);
  const [isEven, setEven] = useState(false);
  const [isFiltered, setFiltered] = useState(false);

  const toDoData = toJS(items);
  const toDoDataFiltered = toDoData.filter((item) => item.completed !== true);
  let data = [];

  if (isFiltered) {
    data = toDoDataFiltered;
  } else {
    data = toDoData;
  };

  const disabledSend = text === '';

  return (
    <>
      <Box>
        <Input
          onChange={(e) => setText(e.currentTarget.value)}
          value={text}
          placeholder="Добавьте задачу"
        />
        <WrapperButton>
          <Button
            disabled={disabledSend}
            onClick={() => {
              addTodoItem(text)
              setText('')
            }}
          >
            Добавить
          </Button>
        </WrapperButton>
      </Box>
      <Wrapper>
        <Span>
          Выделить:
        </Span>
        <Button
          style={{ marginRight: '20px', width: '120px' }}
          onClick={() => setOdd(!isOdd)}
        >
          {!isOdd ? "Не чётные" : <WrapperIcon><SvgClose /></WrapperIcon>}
        </Button>
        <Button
          style={{ width: '100px' }}
          onClick={() => setEven(!isEven)}
        >
          {!isEven ? "Чётные" : <WrapperIcon><SvgClose /></WrapperIcon>}
        </Button>
      </Wrapper>
      <Wrapper>
        <Span>
          Удалить:
        </Span>
        <Button
          style={{ marginRight: '20px' }}
          onClick={() => removeFirstItem()}
        >
          Первый эелемент
        </Button>
        <Button
          onClick={() => removeLastItem()}
        >
          Последний элемент
        </Button>
      </Wrapper>
      <Wrapper>
        <Span>
          Скрыть выполненные задачи:
        </Span>
        <input
          type="checkbox"
          checked={isFiltered}
          style={{ marginRight: '20px', height: '22px', width: '22px' }}
          onChange={() => setFiltered(!isFiltered)}
        />
      </Wrapper>
      {data.length === 0 ?
        (<Wrapper style={{ color: '#797979' }}>
          <Span>
            Начните добавлять задачи
          </Span>
        </Wrapper>)
        :
        (<Container even={String(isEven)} odd={String(isOdd)}>
          {data.map(({ task, id, completed: isCompleted }) =>
            <Item
              key={id}
              removeTodoItem={removeTodoItem}
              updateTodoItem={updateTodoItem}
              completeTodoItem={completeTodoItem}
              sortTodoItem={sortTodoItem}
              task={task}
              id={id}
              isCompleted={isCompleted}
            />
          )}
        </Container>)
      }
    </>
  );
});