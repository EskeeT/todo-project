import { GlobalStyle, Wrapper, Container, H1 } from './styled';
import { Todo } from "../Todo";

export const App = () => (
  <>
    <GlobalStyle />
    <Wrapper >
      <Container>
        <H1>Todo List</H1>
        <Todo />
      </Container>
    </Wrapper>
  </>

);
