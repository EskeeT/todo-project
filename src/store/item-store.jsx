import { makeAutoObservable } from "mobx";

class ItemStore {
  items = [{
    task: 'Task#1',
    id: 0,
    completed: false,
  }];
  allItemsCount = 0;

  constructor() {
    makeAutoObservable(this);
  }

  addTodoItem = (item) => {
    this.allItemsCount += 1;
    this.items.unshift({
      task: item,
      id: this.allItemsCount,
      completed: false,
    });
  };

  removeTodoItem = (id) => {
    this.items = this.items.filter((item) => item.id !== id);
  };

  removeFirstItem = (id) => {
    this.items.shift();
  };

  removeLastItem = (id) => {
    this.items.pop();
  };

  completeTodoItem = (id) => {
    this.items.map((item) => {
      if (id === item.id) {
        item.completed = !item.completed;
      }
      return item;
    })
  };

  sortTodoItem = () => {
    this.items.sort((a, b) => {
      if (!a.completed && b.completed) return -1
      if (a.completed && !b.completed) return 1
      return 0
    });
  };

  updateTodoItem = (updateTask, id) => {
    this.items.forEach((value) => id === value.id ? value.task = updateTask : null);
  };
}

export default new ItemStore();